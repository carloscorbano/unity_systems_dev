﻿using System.Collections.Generic;

public class ThreadSafeQueuePool<T> where T : class, new()
{
    Queue<T> _pool = new Queue<T>();
    object _lock = new object();

    public T Dequeue()
    {
        if(_pool.Count == 0)
            return new T();
        
        lock (_lock)
        {
            return _pool.Dequeue();
        }
    }

    public T Peek()
    {
        return _pool.Peek();
    }

    public void Enqueue(ref T obj)
    {
        _pool.Enqueue(obj);
    }

    public int Count => _pool.Count;
}