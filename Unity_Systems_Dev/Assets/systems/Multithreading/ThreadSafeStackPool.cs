﻿using System.Collections.Generic;

public class ThreadSafeStackPool<T> where T : class, new()
{
    Stack<T> _pool = new Stack<T>();
    object _lock = new object();

    public T Pop()
    {
        if (_pool.Count == 0)
            return new T();

        lock (_lock)
        {
            return _pool.Pop();
        }
    }

    public void Push(ref T obj)
    {
        lock (_lock)
        {
            _pool.Push(obj);
        }
    }

    public int Count => _pool.Count;
}