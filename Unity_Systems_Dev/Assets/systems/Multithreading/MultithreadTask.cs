﻿using System;
using System.Collections.Generic;

public class MultithreadTask
{
    public enum CallbackThread
    {
        UNITY_MAIN_THREAD,
        BACKGROUND_WORKER
    }

    public enum TaskState
    {
        RUNNING,
        FINISHED
    }

    public Queue<MultithreadCommand> commands = new Queue<MultithreadCommand>();
    public Action onFinishCallback = null;
    public CallbackThread callbackThread;
    public TaskState state;

    public MultithreadTask BindAction(MultithreadCommand.ExecutionTarget target, Action action)
    {
        MultithreadCommand cmd = MultithreadManager.Instance.GetCommandPool.Pop();
        cmd.action = action;
        cmd.executionTarget = target;

        commands.Enqueue(cmd);

        return this;
    }

    public void DispatchTask()
    {
        MultithreadTask thisObj = this;
        MultithreadManager.Instance.EnqueueTask(ref thisObj);
    }
}
