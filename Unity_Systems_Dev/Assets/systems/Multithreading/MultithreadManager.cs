using System;
using System.Threading.Tasks;
using UnityEngine;

public class MultithreadManager : MonoBehaviour
{
    bool _dispatcherWorking = false;

    #region MANAGER POOLS
    ThreadSafeStackPool<MultithreadTask> _multithreadTaskPool = new ThreadSafeStackPool<MultithreadTask>();
    ThreadSafeStackPool<MultithreadCommand> _commandTaskPool = new ThreadSafeStackPool<MultithreadCommand>();
    public ThreadSafeStackPool<MultithreadCommand> GetCommandPool { get { return _commandTaskPool; } }
    ThreadSafeQueuePool<MultithreadTask> _dispatchPending = new ThreadSafeQueuePool<MultithreadTask>();
    #endregion
    #region DISPATCHED TASKS PILES
    ThreadSafeQueuePool<MultithreadTask> _unityUpdatePile = new ThreadSafeQueuePool<MultithreadTask>();
    ThreadSafeQueuePool<MultithreadTask> _unityThreadCallbackPile = new ThreadSafeQueuePool<MultithreadTask>();
    ThreadSafeQueuePool<MultithreadTask> _unityFixedUpdatePile = new ThreadSafeQueuePool<MultithreadTask>();
    #endregion

    #region SINGLETON
    static MultithreadManager _instance;
    public static MultithreadManager Instance { get { return _instance; } }

    #endregion
    #region UNITY METHODS
    private void Awake()
    {
        if (_instance == null)
            _instance = this;
        else
        {
            if (_instance != this)
            {
                Destroy(this.gameObject);
                return;
            }
        }
    }

    private void Update()
    {
        if(_dispatchPending.Count > 0 && !_dispatcherWorking)
        {
            _dispatcherWorking = true;
            Task.Run(() => {
                ExecuteDispatcher();
                _dispatcherWorking = false;
            });
        }

        //Execute the update pending list.
        ExecuteUpdatePendingList();
    }

    private void FixedUpdate()
    {
        ExecuteFixedUpdatePendingList();
    }
    #endregion

    void ExecuteUpdatePendingList()
    {
        if (_unityUpdatePile.Count > 0)
        {
            MultithreadTask task = _unityUpdatePile.Dequeue();
            ExecuteCommand(ref task);
        }

        if(_unityThreadCallbackPile.Count > 0)
        {
            MultithreadTask task = _unityThreadCallbackPile.Dequeue();
            task.onFinishCallback();
            task.state = MultithreadTask.TaskState.FINISHED;
            _dispatchPending.Enqueue(ref task);
        }
    }

    void ExecuteFixedUpdatePendingList()
    {
        if (_unityFixedUpdatePile.Count > 0)
        {
            MultithreadTask task = _unityFixedUpdatePile.Dequeue();
            ExecuteCommand(ref task);
        }
    }

    void ExecuteDispatcher()
    {
        if (_dispatchPending.Count > 0)
        {
            MultithreadTask task = _dispatchPending.Dequeue();

            //Check if the state of the task is finished
            if (task.state == MultithreadTask.TaskState.FINISHED)
            {
                _multithreadTaskPool.Push(ref task);
            }
            else
            {
                //if is not finished, then check the command count.
                //if it's zero, then just execute the callback on the selected target thread.
                if (task.commands.Count == 0)
                {
                    //Finished executing commands, calling on finished callback method.
                    if (task.onFinishCallback != null && task.callbackThread == MultithreadTask.CallbackThread.UNITY_MAIN_THREAD)
                        _unityThreadCallbackPile.Enqueue(ref task);
                    else if (task.onFinishCallback != null && task.callbackThread == MultithreadTask.CallbackThread.BACKGROUND_WORKER)
                        Task.Run(() =>
                        {
                            task.onFinishCallback();
                            task.state = MultithreadTask.TaskState.FINISHED;

                            _dispatchPending.Enqueue(ref task);
                        });
                }
                else
                {
                    //There are more commands to execute
                    switch (task.commands.Peek().executionTarget)
                    {
                        case MultithreadCommand.ExecutionTarget.UNITY_MAIN_THREAD_UPDATE_METHOD:
                            _unityUpdatePile.Enqueue(ref task);
                            break;
                        case MultithreadCommand.ExecutionTarget.UNITY_MAIN_THREAD_FIXED_UPDATE_METHOD:
                            _unityFixedUpdatePile.Enqueue(ref task);
                            break;
                        case MultithreadCommand.ExecutionTarget.BACKGROUND_WORKER:
                            Task.Run(() => ExecuteCommand(ref task));
                            break;
                    }
                }
            }
        }
    }

    void ExecuteCommand(ref MultithreadTask task)
    {
        MultithreadCommand cmd = task.commands.Dequeue();
        cmd.action();

        _commandTaskPool.Push(ref cmd);
        _dispatchPending.Enqueue(ref task);
    }

    public MultithreadTask CreateTask(Action callback, MultithreadTask.CallbackThread targetThread)
    {
        MultithreadTask task = _multithreadTaskPool.Pop();
        task.state = MultithreadTask.TaskState.RUNNING;
        task.onFinishCallback = callback;
        task.callbackThread = targetThread;
        return task;
    }

    public void EnqueueTask(ref MultithreadTask task)
    {
        _dispatchPending.Enqueue(ref task);
    }
}