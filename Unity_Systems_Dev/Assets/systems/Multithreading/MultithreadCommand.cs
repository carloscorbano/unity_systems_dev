﻿using System;

public class MultithreadCommand
{
    public enum ExecutionTarget
    {
        UNITY_MAIN_THREAD_UPDATE_METHOD,
        UNITY_MAIN_THREAD_FIXED_UPDATE_METHOD,
        BACKGROUND_WORKER
    }

    public Action action;
    public ExecutionTarget executionTarget;
}